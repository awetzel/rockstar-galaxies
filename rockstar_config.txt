# configuration for Rockstar Halo Finder

# run by first starting server
# ../../../rockstar-galaxies/rockstar-galaxies -c ../../../rockstar-galaxies/rockstar_config.txt &> rockstar_server_log.txt &
# then launch reading/analysis tasks
# ../../../rockstar-galaxies/rockstar-galaxies -c catalog/auto-rockstar.cfg >> rockstar_log.txt &

# restart via
# ../../../rockstar-galaxies/rockstar-galaxies -c catalog/restart.cfg


## input parameters ##
# file format: "AREPO", "GADGET2", "ART", "ASCII"
FILE_FORMAT = "AREPO"

# conversion parameters - Rockstar internal units are [Mpc/h] and [M_sun/h]
AREPO_LENGTH_CONVERSION = 1e-3
AREPO_MASS_CONVERSION = 1e+10
#GADGET_LENGTH_CONVERSION = 1e-3
#GADGET_MASS_CONVERSION = 1e+10

# directory of snapshot files
INBASE = "../../output"

# name of snapshot file - use <block>, <snap> for wildcards
FILENAME = "snapshot_<snap>.hdf5"
#FILENAME = "snapdir_<snap>/snapshot_<snap>.<block>.hdf5"


## run time parameters ##
# force resolution of simulation [Mpc/h]
# ~1 kpc at z = 0 - 1, ~2/(1+z) kpc at z > 1
FORCE_RES = 0.0014    # [Mpc/h comoving]
FORCE_RES_PHYS_MAX = 0.0007    # [Mpc/h physical]

# factor by which to down-weight difference in velocity between DM and non-DM particles
NON_DM_METRIC_SCALING = 30

# initial FoF finder
FOF_FRACTION = 0.7
FOF_LINKING_LENGTH = 0.28

# minimum bounded fraction of halo to keep it
UNBOUND_THRESHOLD = 0.3

# minimum particle count of halo to output it
MIN_HALO_OUTPUT_SIZE = 30

# mass definitions to compute
MASS_DEFINITION = "200b"
MASS_DEFINITION2 = "vir"
MASS_DEFINITION3 = "200c"
MASS_DEFINITION4 = "500c"
MASS_DEFINITION5 = "180b"

EXTRA_PROFILING = 0


## output parameters ##
# directory to write output files
OUTBASE = "catalog"

# format of output files: "ASCII", "BINARY", "BOTH"
OUTPUT_FORMAT = BOTH

# output full particle information - set to number of writer processes
#FULL_PARTICLE_CHUNKS = 8

# ???
SUPPRESS_GALAXIES = 0


## parallel parameters ##
# flag to run on multiple input files, snapshots, or CPUs
PARALLEL_IO = 1

# number of CPUs - set if number of CPUs < number of blocks
#NUM_READERS = 1

# number of files per snapshot
NUM_BLOCKS = 1

# path + name of file that lists snapshot block numbers (still set NUM_BLOCKS)
#BLOCK_NAMES = snapshot_blocks.txt

# number of snapshots
#NUM_SNAPS = 601

# index of first snapshot
#STARTING_SNAP = 1

# path + name of file that lists snapshot file indices (instead of NUM_SNAPS + STARTING_SNAP)
SNAPSHOT_NAMES = snapshot_indices.txt

# number of writer (worker) tasks - set to 1 or multiple of 8 - uses periodic boundaries only if >= 8
NUM_WRITERS = 8

# use to override to set non-periodic if NUM_WRITERS >= 8, or to allow 1 < NUM_WRITERS < 8
PERIODIC = 0

# automatically split off reader tasks so only have to start writer tasks
FORK_READERS_FROM_WRITERS = 1

# automatically split each writer task into many copies and set number of processors per node
FORK_PROCESSORS_PER_MACHINE = 8


## parameters that must specify for ART and/or ASCII ##
# mass of particle [M_sun/h]
#PARTICLE_MASS = 0


## ignore particle ids and do not create halo link file (.list)
#IGNORE_PARTICLE_IDS = 1
